import React from 'react';
import {Platform, View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import PropTypes from 'prop-types';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const Header  = ({ title, isCloseEnable, onClosePress}) => {
    return (
        <View style={styles.headerContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.titleText}>{title}</Text>
            </View>
            {isCloseEnable && <TouchableOpacity 
              onPress={onClosePress}>
              <MaterialCommunityIcon
                name="close-box"
                size={48}
                color={'red'}
                style={styles.closeIcon}/>
            </TouchableOpacity>}

        </View>
    );
}

const styles = StyleSheet.create({
  headerContainer: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: Platform.OS === 'ios' ? 20 : 0,
  },
  confirmIcon: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleContainer: {
    position: 'absolute',
    left: 0, 
    right:0, 
    bottom: 0, 
    top: 0,
    justifyContent: 'center', 
    alignItems: 'center'
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold'
  }
});

Header.propTypes = {
    onClosePress: PropTypes.func,
    isCloseEnable: PropTypes.bool,
  };

Header.defaultProps = {
    title: "Title",
    onClosePress: () => {},
    isCloseEnable: true,
  };

export default Header;
