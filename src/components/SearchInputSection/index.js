import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const SearchInputSection = ({ onChangeText }) => {

    return (
        <View style={styles.searchContainer}>
          <View style={styles.searchInputFieldContainer}>
            <TextInput
              style={styles.searchInput}
              underlineColorAndroid='transparent'
              placeholder="Search..."
              onChangeText={(text) => onChangeText(text)}/>
          </View>
          <View style={{alignItems: 'center', justifyContent: 'center', paddingHorizontal: 16}}>
            <Icon
              name="magnify"
              size={30}
              color={'#000000'}/>
          </View>
        </View>
    );
}

const styles = StyleSheet.create({
  searchContainer: {
    marginBottom: 0,
    flexDirection: 'row',
    borderColor: '#ddd',
    backgroundColor: '#f8f8f8',
    borderWidth: 1,
    height: 57,
    justifyContent: 'space-between',
    alignSelf: 'stretch',
  },
  searchInputFieldContainer: {
    paddingLeft: 32, 
    flex: 1, 
    justifyContent: 'center'
  },
  searchInput: {
    fontSize: 18,
  },
  iconStyle: {
    flex: 1, 
    position: 'absolute', 
    right: '4%', 
    top: '22%',
  }
});

SearchInputSection.propTypes = {
  onChangeText: PropTypes.func
};

export default SearchInputSection;
