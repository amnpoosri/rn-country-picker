import React, {Component} from 'react';
import {Text, View, FlatList, StyleSheet, SafeAreaView} from 'react-native';
import PropTypes from 'prop-types';

import {get, orderBy} from 'lodash';

import Header from '../../components/Header'
import SearchInputSection from '../../components/SearchInputSection'
import RowItem from './RowItem'

class SingleSelection extends Component {

  static propTypes = {
    selectedItem: PropTypes.object,
    items: PropTypes.array.isRequired,
    uniqueKey: PropTypes.string,
    onChangeInput: PropTypes.func,
    displayKey: PropTypes.string,
    onItemSelected: PropTypes.func.isRequired,
    onClosePress: PropTypes.func,
    headerTitle: PropTypes.string,
    searchEmptyTitle: PropTypes.string
  };

  static defaultProps = {
    selectedItem: {},
    items: [],
    uniqueKey: 'id',
    onChangeInput: () => {},
    displayKey: 'name',
    onItemSelected: () => {},
    onClosePress: () => {},
    headerTitle: 'Select item',
    searchEmptyTitle: 'Cannot find the items.'
  };

  constructor(props: PropsType) {
    super(props);
    this.state = {
      searchTerm: '',
    }
  }

  onSearchTextChange = (text) => {
    this.setState({ searchTerm: text });
  }

  isItemSelected = item => {
    const { uniqueKey, selectedItem } = this.props;
    return selectedItem[uniqueKey] === item[uniqueKey]? true : false
  };

  onRowItemClicked = item => {
    const {
      onItemSelected
    } = this.props;

    onItemSelected(item);
  };

  filterItems = searchTerm => {
    const { items, displayKey } = this.props;
    const filteredItems = [];
    items.forEach(item => {
      const parts = searchTerm.trim().split(/[ \-:]+/);
      const regex = new RegExp(`(${parts.join('|')})`, 'ig');
      if (regex.test(get(item, displayKey))) {
        filteredItems.push(item);
      }
    });
    return filteredItems;
  };

  _renderEmptyListComponent = () => {
    return (
        <View style={styles.emptyContainer}>
          <Text
            style={styles.emptyText}>
            {this.props.searchEmptyTitle}
          </Text>
        </View>
    )
  }
  _renderFlatList = (items) => {
    const {
      uniqueKey,
      selectedItem
    } = this.props;
    const { searchTerm } = this.state;
    const renderItems = searchTerm ? 
      this.getOrderItems(this.filterItems(searchTerm)) : 
      items;

    return (<FlatList
          contentContainerStyle={{ flexGrow: 1 }}
          ListEmptyComponent={this._renderEmptyListComponent}
          data={renderItems}
          extraData={selectedItem}
          keyExtractor={item => item[uniqueKey]}
          renderItem={rowData => 
            <RowItem 
              item = {rowData.item}
              onPress = {this.onRowItemClicked}/>
          }/>)
  };
  
  getOrderItems = (items) => {
    return orderBy(items, ['isSelected', 'name'], ['desc', 'asc'])
  }

  getSortedItemChecklist = (items) => {
    var self = this;

    items.forEach(function(item){item.isSelected =  self.isItemSelected(item)});
    return this.getOrderItems(this.props.items)
  }


  render() {

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.singleSelectionContainer}>
          <Header
            title={this.props.headerTitle}
            onClosePress={this.props.onClosePress}
            isConfirmEnable={false}/>
          <SearchInputSection 
            onChangeText={this.onSearchTextChange}/>
          <View style={styles.container}>
            {this._renderFlatList(this.getSortedItemChecklist(this.props.items))}
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  singleSelectionContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'white'
  },
  emptyContainer: { 
    height: '100%', 
    justifyContent: 'center'
  },
  emptyText: {
    fontSize: 16,
    textAlign: 'center',
    color: 'red'
  }
});

export default SingleSelection;
