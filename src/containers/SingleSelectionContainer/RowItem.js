import React from 'react';
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const RowItem = ({ item, onPress}) => {

    return (
      <View> 
        <TouchableOpacity 
          activeOpacity={1}
          style={styles.touchContainer}
          onPress={() => {
            onPress(item)}
            }>
            {/* <View style={{flex: 1, alignItems: 'center'}}> 
            </View> */}
            {item.isSelected? 
              <Icon 
                name="checkbox-marked-circle" 
                size={30} 
                color={'#777777'}
                style={styles.iconStyle}>
              </Icon> : <Icon 
                name="checkbox-blank-circle-outline" 
                size={30} 
                color={'#D3D3D3'}
                style={styles.iconStyle}>
              </Icon>
            }

            <View style={styles.textContainer}>
              {item.isSelected? 
                (<Text 
                  style={styles.selectedText}>
                  {`${item.name}`}
                </Text>) :
                (<Text 
                  style={styles.unselectedText}>
                  {`${item.name}`}
                  </Text>)
              }
            </View>
          </TouchableOpacity>
        </View>
   
    );
  }

const styles = StyleSheet.create({
  touchContainer: {
    paddingVertical: 10,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderStyle: 'solid',
    borderColor: '#ecf0f1'
  },
  iconStyle: {
    marginHorizontal: 32
  },
  textContainer: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  selectedText: {
    fontWeight: 'bold', 
    color: '#777777'
  },
  unselectedText: {
    color: '#777777'
  }

});

RowItem.propTypes = {
  item: PropTypes.object,
  onPress: PropTypes.func
};

export default RowItem;
