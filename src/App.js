import React, {Component} from 'react';
import { StyleSheet, View, TouchableHighlight, Text} from 'react-native';
import Modal from 'react-native-modal'
import {isEmpty} from 'lodash';

import SingleSelectionContainer from './containers/SingleSelectionContainer';
const countryList = require('./constants/country.json'); 
  

export default class App extends Component {

    static propTypes = {

      };

    constructor(props: PropsType) {
        super(props);
        this.state = {
            isSingleSelectionVisible: false,
            selectedItem: {}
          }  
      }

    onItemSelected = selectedItem => {
        this.setState({ selectedItem });
        this.hideSingleSelectionModal()
      };

    showSingleSelectionModal = () => {
        this.setState({ isSingleSelectionVisible: true });
      };

    hideSingleSelectionModal = () => {
        this.setState({ isSingleSelectionVisible: false });
      };  


  render() {
    return (
      <View style={styles.container}>
        {/* Item */}
        <View style={styles.itemContainer}>
            <TouchableHighlight 
                onPress={this.showSingleSelectionModal} 
                underlayColor='white'
                style={styles.button}>
              <View style={styles.itemInlineButton}>
                <Text style={styles.itemText}>
                  {isEmpty(this.state.selectedItem) ? 'Select your country': this.state.selectedItem.name}
                </Text>
              </View>
            </TouchableHighlight>
        </View>
        
        {/* Single Selection Modal */}
       <Modal
        isVisible={this.state.isSingleSelectionVisible}
        style={styles.singleSelectionModal}
        onBackButtonPress={this.hideSingleSelectionModal}>
        <SingleSelectionContainer
          headerTitle={"Select your country"}
          searchEmptyTitle={"Cannot country."}
          items={countryList}
          uniqueKey="name"
          displayKey="name"
          selectedItem={this.state.selectedItem}
          onItemSelected={this.onItemSelected}
          onClosePress={this.hideSingleSelectionModal}/>
        </Modal>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  button: {
    flex: 1,
    alignSelf: 'stretch',
  },
  itemContainer: {
    borderColor: '#ddd',
    backgroundColor: '#f8f8f8',
    borderWidth: 1,
    margin: -1,
    height: 47,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemInlineButton: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  itemText: {
    color: '#79797a',
    fontSize: 14,
    fontWeight: 'bold',
  },
  singleSelectionModal: {
    backgroundColor: 'white',
    margin: 0, // This is the important style you need to set
    alignItems: undefined,
    justifyContent: undefined,
  }

});
